#pragma once

typedef enum
{
	NODE_KIND_ADD,
	NODE_KIND_SUBTRACT,
	NODE_KIND_MULTIPLY,
	NODE_KIND_DIVIDE,
	NODE_KIND_DIGIT
} NodeKind;

typedef struct Node Node;

struct Node
{
	NodeKind	kind;
	Node*		left;
	Node*		right;
	int			value;
};

typedef struct Token Token;

Node* node_parse(Token** remaining, Token* token);
