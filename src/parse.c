#include <stdio.h>
#include <stdlib.h>

#include "parse.h"
#include "token.h"

static
Node* node_new(NodeKind kind)
{
	Node *node = calloc(1, sizeof(Node));
	node->kind = kind;

	return node;
}

static
Node* node_new_binary(NodeKind kind, Node* left, Node* right)
{
	Node* node = node_new(kind);
	node->left = left;
	node->right = right;

	return node;
}

static
Node* node_new_digit(int value)
{
	Node* node = node_new(NODE_KIND_DIGIT);
	node->value = value;

	return node;
}

static
Node* node_primary(Token** remaining, Token* token)
{
	if (token_equal(token, "("))
	{
		Node* node = node_parse(&token, token->next);
		*remaining = token_skip(token, ")");
		return node;
	}

	if (token->kind == TOKEN_KIND_DIGIT)
	{
		Node* node = node_new_digit(token->value);
		*remaining = token->next;
		return node;
	}

	fprintf(stderr, "Expected an expression.\n");
	exit(1);
}

static
Node* node_multiply(Token** remaining, Token* token)
{
	Node* node = node_primary(&token, token);

	while (1)
	{
		if (token_equal(token, "*"))
		{
			node = node_new_binary(NODE_KIND_MULTIPLY, node, node_primary(&token, token->next));
			continue;
		}

		if (token_equal(token, "/"))
		{
			node = node_new_binary(NODE_KIND_DIVIDE, node, node_primary(&token, token->next));
			continue;
		}

		*remaining = token;

		return node;
	}
}

Node* node_parse(Token** remaining, Token* token)
{
	Node* node = node_multiply(&token, token);

	while (1)
	{
		if (token_equal(token, "+"))
		{
			node = node_new_binary(NODE_KIND_ADD, node, node_multiply(&token, token->next));
			continue;
		}

		if (token_equal(token, "-"))
		{
			node = node_new_binary(NODE_KIND_SUBTRACT, node, node_multiply(&token, token->next));
			continue;
		}

		*remaining = token;

		return node;
	}
}
