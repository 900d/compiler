#include <stdio.h>
#include <stdlib.h>

#include "token.h"

static
Token* token_new(TokenKind kind, char* start, char* end)
{
	Token* token = calloc(1, sizeof(Token));
	token->kind = kind;
	token->location = start;
	token->length = (int)(end - start);

	return token;
}

bool token_equal(Token* token, char* input)
{
	return memcmp(token->location, input, token->length) == 0 && input[token->length] == '\0';
}

Token* token_skip(Token* token, char* input)
{
	if (!token_equal(token, input))
	{
		fprintf(stderr, "ERROR: Expected %s", input);
		exit(1);
	}

	return token->next;
}

static
bool token_is_space(int c)
{
	return c == ' ' || (unsigned int)c - '\t' < 5;
}

static
bool token_is_digit(int c)
{
	return (unsigned int)c - '0' < 10;
}

static
bool token_is_alpha(int c)
{
	return ((unsigned int)c | 32) - 'a' < 26;
}

static
bool token_is_graph(int c)
{
	return (unsigned int)c - 0x21 < 0x5E;
}

static
bool token_is_punct(int c)
{
	return token_is_graph(c) && !(token_is_alpha(c) || token_is_digit(c));
}

Token* token_tokenize(char* input)
{
	Token head = { 0 };
	Token* cursor = &head;

	while (*input)
	{
		// Skip whitespace
		if (token_is_space(*input))
		{
			input++;
			continue;
		}

		// Digits { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }
		if (token_is_digit(*input))
		{
			cursor = cursor->next = token_new(TOKEN_KIND_DIGIT, input, input);

			char* temp = input;
			cursor->value = strtoul(input, &input, 10);
			cursor->length = (int)(input - temp);
			continue;
		}

		// Punctuator { +, -, *, /, (, ) }
		if (token_is_punct(*input))
		{
			cursor = cursor->next = token_new(TOKEN_KIND_PUNCTUATOR, input, input + 1);
			input++;
			continue;
		}

		fprintf(stderr, "ERROR: Invalid token.\n");
		exit(1);
	}

	cursor = cursor->next = token_new(TOKEN_KIND_EOF, input, input);

	return head.next;
}
