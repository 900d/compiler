#include <stdio.h>

#include <string.h>

#include "compiler.h"

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		return 2;
	}

	Token* token = token_tokenize(argv[1]);

	Node* node = node_parse(&token, token);

	codegen_generate(node);

	return 0;
}
