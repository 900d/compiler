#pragma once

#include <stdbool.h>

typedef enum TokenKind
{
	TOKEN_KIND_PUNCTUATOR,
	TOKEN_KIND_DIGIT,
	TOKEN_KIND_EOF
} TokenKind;

typedef struct Token Token;

struct Token
{
	TokenKind	kind;
	Token*		next;
	int			value;
	char*		location;
	int			length;
};

bool token_equal(Token* token, char* input);

Token* token_skip(Token* token, char* input);

Token* token_tokenize(char* input);
