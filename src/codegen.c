#include <stdio.h>
#include <stdlib.h>

#include "codegen.h"
#include "parse.h"

static int Depth;

static
void codegen_push()
{
	fprintf(stdout, "\tpush rax\n");
	Depth++;
}

static
void codegen_pop(char* reg)
{
	fprintf(stdout, "\tpop %s\n", reg);
	Depth--;
}

static
void codegen_generate_expression(Node* node)
{
	if (node->kind == NODE_KIND_DIGIT)
	{
		fprintf(stdout, "\tmov rax, %d\n", node->value);
		return;
	}

	codegen_generate_expression(node->right);
	codegen_push();
	codegen_generate_expression(node->left);
	codegen_pop("rdi");

	switch (node->kind)
	{
	case NODE_KIND_ADD:
		fprintf(stdout, "\tadd rax, rdi\n");
		break;

	case NODE_KIND_SUBTRACT:
		fprintf(stdout, "\tsub rax, rdi\n");
		break;

	case NODE_KIND_MULTIPLY:
		fprintf(stdout, "\timul rax, rdi\n");
		break;

	case NODE_KIND_DIVIDE:
		fprintf(stdout, "\tcqo\n");
		fprintf(stdout, "\tidiv rdi\n");
		break;

	default:
		fprintf(stderr, "Unhandled node kind.\n");
		exit(1);
	}
}

void codegen_generate(Node* node)
{
	fprintf(stdout,
			".CODE\n"
			"main PROC\n");

	codegen_generate_expression(node);

	fprintf(stdout,
			"\tret\n"
			"main ENDP\n"
			"\nEND\n");
}